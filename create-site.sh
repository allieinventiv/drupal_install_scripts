#!/bin/bash

CURRENT_USER=`whoami`
DONT_PANIC_DIR=`pwd`
#pwd present working directory 
source ./functions.sh #pulls in functions script
export LC_CTYPE=C 
export LANG=C
#http://stackoverflow.com/questions/19242275/re-error-illegal-byte-sequence-on-mac-os-x

#First check if user has git, if so update dont panic template
if [ -d .git ] ; then
git pull
fi

echo "Enter the directory where you would like to keep your site" 
read YOUR_DIR
export YOUR_DIR=$YOUR_DIR

echo "Enter full domain with extension (example mysite.com)"
read FULL_DOMAIN

FULL_DOMAIN=`echo $FULL_DOMAIN | tr '[:upper:]' '[:lower:]'`
#change domain string to be all lowercase
echo $FULL_DOMAIN

export SITE_NAME=${FULL_DOMAIN%.*}
#everything from the left without the dot

TLD=.${FULL_DOMAIN#*.}
#Top Level Domain - everything to the right with a leading dot

SITE_DIR=$YOUR_DIR/$SITE_NAME

if [ -d $SITE_DIR ]
then
    echo "$SITE_DIR already exists. If you mean create it again please delete $SITE_DIR folder and run this script again. Terminating."
    exit
fi

#Database variables must be less than 16 characters truncate the string if necessary
DB_VAR=`echo ${SITE_NAME:0:15}`

DB_NAME=$DB_VAR
echo "Your database name was generated for you $DB_NAME"

DB_USER=$DB_VAR
echo "Your database user was generated for you $DB_USER"

#Decide if user is running a Unix shell or OSX
DB_PASS=$(gen_password 20 1)

echo "Your secure database password was generated for you \"$DB_PASS\""

echo "Now, please enter your local password to continue"
mkdir -p $SITE_DIR/data/sql

rsync -a --delete $DONT_PANIC_DIR/data/ $SITE_DIR/data/
#Find and replace all instances of templatesite" with $sitename
#http://stackoverflow.com/questions/6178498/using-grep-and-sed-to-find-and-replace-a-string
#http://stackoverflow.com/questions/7573368/in-place-edits-with-sed-on-os-x
find $SITE_DIR/data -type f -exec sed -i "" "s/templatesitedomain/$SITE_NAME$TLD/g" {} \;
find $SITE_DIR/data -type f -exec sed -i "" "s/templatesite/$SITE_NAME/g" {} \;
find $SITE_DIR/data -type f -exec sed -i "" "s/DB_USER/$DB_USER/g" {} \;
find $SITE_DIR/data -type f -exec sed -i "" "s/DB_PASS/$DB_PASS/g" {} \;
find $SITE_DIR/data -type f -exec sed -i "" "s/DB_NAME/$DB_NAME/g" {} \;
find $SITE_DIR/data -type f -exec sed -i "" "s/DB_NAME/$DB_NAME/g" {} \;

#remove forward slash to allow sed to work. Will add the forward slashes back in
TEMP_SITE_DIR="${SITE_DIR//\//slash}"
find $SITE_DIR/data -type f -exec sed -i "" "s/SITE_DIR/$TEMP_SITE_DIR/g" {} \;
#replace the $$ tokens back to forward slash to restore path
#http://forums.devshed.com/unix-help-35/sed-escaping-forward-slash-393115.html
find $SITE_DIR/data -type f -exec sed -i "" "s#slash#\/#g" {} \;

cd $SITE_DIR/data
SITENAME=$SITE_NAME
DB_USER=$DB_USER
DB_PASS=$DB_PASS
DB_NAME=$DB_NAME
ADMIN_USER=admin
ADMIN_EMAIL=admin@$SITE_NAME.com
ADMIN_PASS=$(gen_password 20 1)

#provide user a reminder of username and password
find $SITE_DIR/data -type f -exec sed -i "" "s/ADMIN_USER/$ADMIN_USER/g" {} \;
find $SITE_DIR/data -type f -exec sed -i "" "s/ADMIN_PASS/$ADMIN_PASS/g" {} \;


#make file executable by the user & Run db user script
chmod u+x ./create-db-user.sh
./create-db-user.sh $DB_NAME $DB_USER $DB_PASS

cd $SITE_DIR/data
source site-variables   

sudo rm -f /etc/apache2/sites-enabled/$SITE_NAME
sudo cp virtualhost.txt /etc/apache2/sites-available/$SITE_NAME
cd /etc/apache2/sites-enabled
sudo ln -s ../sites-available/$SITE_NAME $SITE_NAME
echo "Created apache virtual host /etc/apache2/sites-enabled/$SITE_NAME"
mkdir -p $SITE_DIR/logs
chmod 777 $SITE_DIR/logs
echo "Created log directory $SITE_DIR"
sudo apachectl restart

#Add the entry to /etc/hosts
#http://stackoverflow.com/questions/11287861/how-to-check-if-a-file-contains-a-specific-string-using-bash
HOST_ENTRY="127.0.0.1 $SITENAME.dev"
if grep -q "$HOST_ENTRY" "/etc/hosts"; then
   echo "$SITENAME.dev already exists in /etc/hosts"
else
  #https://blogs.oracle.com/joshis/entry/sudo_echo_does_not_work
  sudo sh -c "echo $HOST_ENTRY >> /etc/hosts"
  echo "Added $SITENAME.dev to /etc/hosts file"
fi
# >> appends to the end of a file 


#Download Drush if it does not exist yet
if hash drush 2>/dev/null; then
       echo "Drupal master you already have drush, continue."
    else
        echo "Drush is being installed for you"
       ./download-drush.sh
    fi

#Download Drupal if it does not exist yet
if [ ! -d $SITE_DIR/web ]
then
  echo "Downloading Drupal"
  cd $SITE_DIR
  drush dl drupal
  mv drupal-* web
  echo "Drupal downloaded successfully."
fi


cd $SITE_DIR/web
drush si standard -y --db-url=mysql://$DB_USER:$DB_PASS@localhost/$DB_NAME --db-su=root --account-mail=$ADMIN_EMAIL --account-name=$ADMIN_USER --account-pass=$ADMIN_PASS --site-name=$SITE_NAME

#Create the folder for features
mkdir -p $SITE_DIR/web/sites/all/feats
touch $SITE_DIR/web/sites/all/feats/.gitignore

#Create the folders for modules. Some personal preferences for folder structure
mkdir -p $SITE_DIR/web/sites/all/modules/contrib
touch $SITE_DIR/web/sites/all/modules/contrib/.gitignore
mkdir -p $SITE_DIR/web/sites/all/modules/custom
touch $SITE_DIR/web/sites/all/modules/custom/.gitignore
mkdir -p $SITE_DIR/web/sites/all/modules/patched
touch $SITE_DIR/web/sites/all/modules/patched/.gitignore
mkdir -p $SITE_DIR/web/sites/all/libraries
rsync -avz --exclude=.svn $YOUR_DIR/drupal-template-site/web/sites/all/libraries/ $SITE_DIR/web/sites/all/libraries/

#sets caching | downloads omega | creates a subtheme | configures sass dependencies
cd $SITE_DIR/data
chmod u+x config-drupal
./config-drupal


#create an sql folder and then create a backup of the database
cd $SITE_DIR/data
mkdir sql



echo "Look how awesome you are! Setup complete.  The $SITE_NAME is installed at $SITE_DIR."

#source $SITE_DIR/data/site-config
open "http://$SITE_NAME.dev"
open  "login-info.txt"
