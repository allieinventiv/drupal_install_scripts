#http://www.linuxjournal.com/content/return-values-bash-functions
#use local prefix so variable lifetime is created within the function and destroyed when function exits 

function gen_password() {
local LENGTH=$1  #how long = length of password
local CASES=$2   #how strong
local CHARACTERS="A-Za-z0-9_\!\@\#\$\%\^\&\*\-+"

case "$CASES" in 
    1)
   local CHARACTERS="A-Za-z0-9";;
esac #end case 

if [ "`which uname /dev/null 2>&1`" ]; then
  case "`uname -a`" in
    Darwin*)
    local DB_PASS=`LC_CTYPE=C tr -dc $CHARACTERS < /dev/urandom | head -c $LENGTH | xargs` ;;
    *)
    local DB_PASS=`tr -dc $CHARACTERS < /dev/urandom | head -c $LENGTH | xargs` ;;
  esac  
fi

echo $DB_PASS
}

