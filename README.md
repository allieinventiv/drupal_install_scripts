
<p>||******************** Drupal Site Configuration with Bash ****************************||</p>

Configure a local environment of your Drupal site for testing and development from one bash script. This script will automate your entire site installation by creating your virtualhost file, adding your [sitename] to /etc/hosts, creating the MYSQL database, downloading (if necessary) Drush, Drupal 7, Omega base theme, and all sass dependancies. All you have to do is run ./create-site.sh and answer two questions that follow in your terminal. Let the script do the rest of the work for you.<br>
Don't Panic, you can do this!!

**Questions You will need to answer**<br>
1.Enter the directory where you would like to keep your site<br>
example: /opt/dev/projects<br>
2.Enter full domain with extension (example mysite.com)<br>
example: dontpanic.com<br>

That's it. Then let Bash do the rest.<br>



**Requirements for these scripts to work**<br>
[MYSQL](http://www.mysql.com/)<br>
[Apache](http://httpd.apache.org/)<br>
[PHP](http://www.php.net/)<br>


**What these scripts do not do**<br>
Will not create your site if you do not set the correct variables.<br>
Will not work if you do not have apache and MYSQL.<br>
Will not give you a gui to point and click your way through configuration.<br> 


**What these scripts actually do**<br>
Will cut your site installation and configuration time down.<br> 
Will automate your site setup and configuration.<br>
Once set-up, expect a feeling of euphoria that comes from automating tasks.<br>


**Instructions** <br>
 Download Files from Github <br>
    Open your terminal <br> 
    run script <br>
        ./create-site.sh <br>
    Answer questions as necessary  <br>

The script with then<br> 
Create your folder pathing<br>
Setup your site's data folder<br>
Configure your virtual host<br>
Add the 127.0.0.1 [sitename].dev entry to /etc/hosts<br>
Create your MYSQL Database<br>
Download composer and Drush if necessary<br>
Download Drupal 7 to the web folder<br>
Run the drush site install command to setup the Drupal 7 site<br>
Run drush to install a bunch of standard modules I think are worth having out of the box<br>
Install omega theme<br>
Create a base theme off of omega<br>
Download Bundler for sass dependencies<br>
Open your site in your default browser<br>
Open a text file of your Drupal login information.<br>

************************************

Credit due to Mark Stralka and Ankur Patel for patiently guiding me through this bash wormhole.


42
