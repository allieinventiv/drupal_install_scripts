<VirtualHost *:80>
UseCanonicalName off

ServerName templatesite.dev

DocumentRoot SITE_DIR/web
ErrorLog SITE_DIR/logs/error_log
CustomLog SITE_DIR/logs/access_log combined

# MIME Type for IE CSS Fixes
AddType text/x-component .htc

RewriteEngine on

<Directory "SITE_DIR/web">
Options FollowSymLinks
AllowOverride All
Allow from All
</Directory>

<Directory "SITE_DIR/web/sites/default/files">
php_flag engine off
</Directory>
</VirtualHost>