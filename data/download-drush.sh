#!/bin/bash
#This script will only run if the user does not already have drush installed.


#curl -sS https://getcomposer.org/installer | php

PHP_VAR=$(php -v | awk 'NR==1 {print $2}')
#http://stackoverflow.com/questions/14971102/print-specific-field-from-first-line-in-file-using-awk

PHP_VER=`echo ${PHP_VAR:2:1}`

#check version of php if 5.3 or higher download composer. If you already have composer download drush.
#http://stackoverflow.com/questions/14511295/bash-integer-comparison
if (( $PHP_VER >= 3 )); 
    then
    if hash composer 2>/dev/null;
    then
        echo "You already have composer"   
    else
        echo "$PHP_VAR is greater than 5.2 downloading composer"
        curl -sS https://getcomposer.org/installer | php
    fi
    if hash drush 2>/dev/null;
        then
        echo "drush installed"
        else
          composer global require drush/drush:dev-master
        fi
fi

#check version of php if 5.2 or lower download composer. If you already have composer download drush.
#http://stackoverflow.com/questions/14511295/bash-integer-comparison
if (( $PHP_VER <= 2 )); 
    then
    if hash drush 2>/dev/null;
        then
        echo "drush installed"
        else
          echo "You must download drush https://github.com/drush-ops/drush"
        fi
fi
